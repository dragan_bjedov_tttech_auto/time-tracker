# README #

Firefox web extension for TimeTracker.

### How to use it ###
Click on clock icon in toolbar and time for week will be populated.

### How to build ###
First clone repository.
```
git clone git clone git@bitbucket.org:dragan_bjedov_tttech_auto/time-tracker.git

cd time-tracker
```

### Build extension
```
web-ext build
```

### Pack extension
```
web-ext sign --api-key=$AMO_JWT_ISSUER --api-secret=$AMO_JWT_SECRET
```
The API options are required to specify your [addons.mozilla.org](https://addons.mozilla.org/en-US/developers/addon/api/key/) credentials.

- `--api-key`: the API key (JWT issuer) from addons.mozilla.org needed to sign the extension. This should always be a string.
- `--api-secret`: the API secret (JWT secret) from addons.mozilla.org needed to sign the extension. This should always be a string.
