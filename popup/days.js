/**
 * Listen for clicks on the buttons, and send the appropriate message to
 * the content script in the page.
 */
function listenForClicks() {
    console.info("Dragan -> addEventListener")
    document.addEventListener("click", (e) => {
        function fillDay(tabs) {
            if (!tabs[0].url.startsWith("https://timetracking.tttech.com/daily_record")) {
                browser.tabs.create({
                    url: "https://timetracking.tttech.com/daily_record?:action=daily_record_action&:template=edit"
                });
            } else {
                var mobile = document.getElementById("work_location_mobile").checked;
                browser.tabs.sendMessage(tabs[0].id, {
                    command: e.target.textContent + "_" + mobile
                });
            }
        }

        console.info("CLICK")
        if (e.target.textContent == "All"
            || e.target.textContent == "Monday"
            || e.target.textContent == "Tuesday"
            || e.target.textContent == "Wednesday"
            || e.target.textContent == "Thursday"
            || e.target.textContent == "Friday") {
            browser.tabs.query({ active: true, currentWindow: true })
                .then(fillDay);
        }
    });
}

/**
 * When the popup loads, inject a content script into the active tab,
 * and add a click handler.
 */
browser.tabs.executeScript(null, { file: "/scripts/main.js" })
    .then(listenForClicks);
