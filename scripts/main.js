function fillTimeTracker(index, mobile) {
    var start_index = 0;
    var end_index = 10;

    if (index != -1) {
        start_index = index;
        end_index = index + 2;
    } else {
        start_index = 0;
    }
    var wps = document.getElementsByClassName("select2-selection__rendered");
    var previous_wp_index = 0;
    for (var i = start_index; i < end_index; i++) {
        var start = document.getElementsByName("time_record-" + (i + 1) + "@start")[0];
        var end = document.getElementsByName("time_record-" + (i + 1) + "@end")[0];
        if (i % 2 == 0) {
            start.value = mobile ? "09:30" : "09:30";
            end.value = "12:00";
        } else {
            start.value = "12:30";
            end.value = mobile ? "18:00" : "18:00";
        }
        document.getElementsByName("time_record-" + (i + 1) + "@time_activity")[0].value = "18";
        document.getElementsByName("time_record-" + (i + 1) + "@work_location")[0].value = mobile ? "2" : "1";
        document.getElementsByName("time_record-" + (i + 1) + "@wp")[0].value = "47001";

        for (var j = previous_wp_index; j < wps.length; j++) {
            // console.info("WPS Index = " + j + "/" + wps.length);
            if (wps[j].id.startsWith("select2-time_record-" + (i + 1) + "@")) {
                if (wps[j].innerHTML == "- no selection -")
                    wps[j].innerHTML = "mPAD Integration  SW-Development_Savic";
                    wps[j].title     = "mPAD Integration  SW-Development_Savic"
                previous_wp_index = j + 1;
                // console.info("Found");
                break;
            }
        }
    }
}

browser.runtime.onMessage.addListener((message) => {
    console.info("TimeTracker Add-on Received command: " + message.command);
    var split = message.command.split("_");
    var day = split[0];
    var mobile = split[1] === "true";
    if (day === "All") {
        fillTimeTracker(-1, mobile);
    } else if (day === "Monday") {
        fillTimeTracker(0, mobile);
    } else if (day === "Tuesday") {
        fillTimeTracker(2, mobile);
    } else if (day === "Wednesday") {
        fillTimeTracker(4, mobile);
    } else if (day === "Thursday") {
        fillTimeTracker(6, mobile);
    } else if (day === "Friday") {
        fillTimeTracker(8, mobile);
    }
});
